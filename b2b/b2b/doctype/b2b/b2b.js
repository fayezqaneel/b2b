// Copyright (c) 2017, Fayez Qandeel and contributors
// For license information, please see license.txt

frappe.ui.form.on('B2B', {
	refresh: function(frm) {
	    frappe.dynamic_link = {doc: frm.doc, fieldname: 'name', doctype: 'B2B'}
		frappe.contacts.render_address_and_contact(frm);
		frm.add_custom_button(__('Make Opportunity'), function () {
			frappe.route_options = {
				"enquiry_from": "Customer",
			};
			frappe.set_route("Form", "Opportunity","New Opportunity 1");
		});
	}
});
