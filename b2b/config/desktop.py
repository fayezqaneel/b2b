# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from frappe import _

def get_data():
	return [
		{
			"module_name": "B2B",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "link",
			"link": "List/B2B",
			"label": _("B2B")
		}
	]
